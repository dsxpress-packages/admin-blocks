<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <title>{{ $page->metaTitle ?? 'Page title' }}</title>
    <meta name="description" content="{{ $page->metaDescription ?? 'Page description'}}">
    <meta name="keywords" content="{{ $page->metaKeywords ?? 'Page meta keywords'}}">
</head>
<body>
    @foreach($page->blocks as $block)
        @include($block->blockable->getView(), [$block->blockable->getVariable() => $block->blockable])
    @endforeach
</body>
</html>

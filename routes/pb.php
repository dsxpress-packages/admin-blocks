<?php

/*
|--------------------------------------------------------------------------
| Page block Routes
|--------------------------------------------------------------------------
|
| This file is where you may override any of the routes that are included
| with Page block.
|
*/
$namespace = 'Pilyavskiy\PB\Http\Controllers';

Route::middleware(['middleware' => 'web'])->namespace($namespace)->group(function () {
    Route::fallback('PageBlockController@show');
});

<?php

namespace Pilyavskiy\PB;

use Pilyavskiy\PB\Console\PBModelMakeCommand;
use Illuminate\Support\ServiceProvider;

class PBServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__.'/../../resources/views'), 'page-blocks');
        $this->loadMigrationsFrom(realpath(__DIR__.'/../../migrations'));
        $this->loadRoutesFrom(realpath(__DIR__.'/../../routes/pb.php'));

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/page-blocks'),
            __DIR__.'/../../migrations' => database_path('migrations'),
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                PBModelMakeCommand::class,
            ]);
        }
    }

    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(__DIR__.'/Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }
}


<?php

namespace Pilyavskiy\PB\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function blocks()
    {
        return $this->hasMany('Pilyavskiy\PB\Model\PageBlock')->orderBy('sortingOrder', 'ASC');;
    }

    public static function getPage(string $slug): ?self
    {
        $page = static::where('page', $slug)->first();

        return $page ?? null;
    }

    public static function getPageByRoute(string $route): ?self
    {
        $page = static::where('route', $route)->first();

        return $page ?? null;
    }
}

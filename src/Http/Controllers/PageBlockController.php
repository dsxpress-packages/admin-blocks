<?php

namespace Pilyavskiy\PB\Http\Controllers;

use Pilyavskiy\PB\Model\Page;
use App\Http\Controllers\Controller;

class PageBlockController extends Controller
{
    public function show(string $slug = null)
    {
        if (empty($slug)) {
            $slug = '/';
        }

        $page = Page::getPageByRoute($slug);
        if (empty($page)) {
            abort(404);
        }

        return view('page-blocks::default', [
            'page' => $page
        ]);
    }
}

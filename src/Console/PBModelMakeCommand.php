<?php

namespace Pilyavskiy\PB\Console;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Foundation\Console\ModelMakeCommand;

class PBModelMakeCommand extends ModelMakeCommand
{
    const VIEW_DIR = 'blocks';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:pbmodel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Page block eloquent model class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Model';


    public function handle()
    {
        if (parent::handle() === false && ! $this->option('force')) {
            return false;
        }

        if ($this->option('block')) {
            $this->createView();
            $this->createMigration();
        }

        if ($this->option('view')) {
            $this->createView();
        }
    }

    protected function createView()
    {
        if (!file_exists(resource_path('views/'.static::VIEW_DIR))) {
            mkdir(resource_path('views/'.static::VIEW_DIR), 0777, true);
        }

        $viewPath = resource_path('views/'.static::VIEW_DIR.'/'.$this->getNameByPath($this->argument('name')).'.blade.php');
        if (!file_exists($viewPath)) {
            $file = fopen($viewPath, 'w');
            fwrite($file, "{{-- Please, use \"{$this->getVarByPath($this->argument('name'))}\" variable! --}}");
            fclose($file);
        }

        $this->line("<info>Created View:</info> {$viewPath}");
    }

    /**
     * Build the class with the given name.
     *
     * @param string $name
     *
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->replaceViewVariable($stub, $name)
            ->replaceView($stub, $name)
            ->replaceNamespace($stub, $name)
            ->replaceClass($stub, $name);
    }

    /**
     * @param $stub
     * @param $name
     * @return $this
     */
    protected function replaceView(&$stub, $name)
    {
        $view = static::VIEW_DIR.'.'.$this->getNameByPath($name);
        $stub = str_replace('DummyView', $view, $stub);

        return $this;
    }

    /**
     * @param $stub
     * @param $name
     * @return $this
     */
    protected function replaceViewVariable(&$stub, $name)
    {
        $variable = $this->getVarByPath($name);
        $stub = str_replace('DummyViewVariable', $variable, $stub);

        return $this;
    }

    private function getNameByPath($path): string
    {
        $path = str_replace($this->getNamespace($path).'\\', '', $path);
        $path = str_replace('\\', '', $path);
        $name = Str::kebab($path);

        return $name;
    }

    private function getVarByPath($path): string
    {
        $path = str_replace($this->getNamespace($path).'\\', '', $path);
        $path = lcfirst($path);
        $name = Str::camel($path) . 'Variable';

        return $name;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/pbmodel.stub';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['block', 'b', InputOption::VALUE_NONE, 'Generate a migration, view, and resource controller for the page block model'],

            ['all', 'a', InputOption::VALUE_NONE, 'Generate a migration, factory, and resource controller for the page block model']
            ,
            ['view', 'w', InputOption::VALUE_NONE, 'Generate a view for the page block model'],

            ['controller', 'c', InputOption::VALUE_NONE, 'Create a new controller for the page block model'],

            ['factory', 'f', InputOption::VALUE_NONE, 'Create a new factory for the model'],

            ['force', null, InputOption::VALUE_NONE, 'Create the class even if the model already exists'],

            ['migration', 'm', InputOption::VALUE_NONE, 'Create a new migration file for the page block model'],

            ['pivot', 'p', InputOption::VALUE_NONE, 'Indicates if the generated model should be a custom intermediate table model'],

            ['resource', 'r', InputOption::VALUE_NONE, 'Indicates if the generated controller should be a resource controller'],
        ];
    }
}

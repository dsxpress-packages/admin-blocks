<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page')->unique();
            $table->string('route')->unique();
            $table->string('title');
            $table->string('metaTitle');
            $table->text('metaDescription')->nullable();
            $table->text('metaKeywords')->nullable();
            $table->boolean('isModified')->default(true);
            $table->boolean('isDeleted')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
